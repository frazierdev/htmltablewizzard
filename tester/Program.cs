﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;

namespace tester
{
    class Program
    {
        static void Main(string[] args)
        {
            HtmlTableWizzard.TableWizz wizz = new HtmlTableWizzard.TableWizz();

            List<string> headers = new List<string> { "header1", "header2", "header3", "header4", "header5" };
            List<List<string>> rows = new List<List<string>> { new List<string> { "data1", "data2", "data3", "data4", "data5" }, new List<string> { "data1", "data2", "data3", "data4", "data5" } };
            List<List<string>> footers = new List<List<string>> { new List<string> { "", "", "", "TOTAL", "100" }, new List<string> { "data1", "data2", "data3", "data4", "data5" } };

            Console.WriteLine(wizz.TableCssBasic() + wizz.GenerateStandardTable(headers, rows, footers));

            //MailAddress a = new MailAddress("Jeremy Westbrook < jwestbrook@frazier.com >");
            //Console.WriteLine(a.DisplayName);
            //Console.WriteLine(a.Address);


            Console.WriteLine("Press Any Key To Exit");
            Console.ReadKey();
        }
    }
}
