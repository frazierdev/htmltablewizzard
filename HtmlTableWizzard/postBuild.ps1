# Post build script to update package nuspec file and deploy nuget
# Place in project root folder
# nuget.exe must be accessible in your PATH system variable
# Frazier nuget repo must be setup
# Place in project post build property: 
# powershell -NoProfile -ExecutionPolicy RemoteSigned -file $(ProjectDir)postBuild.ps1 -projPath $(ProjectDir) -config "$(ConfigurationName)" -projFileName "$(ProjectFileName)" -projName "$(ProjectName)"

Param(
    [string]$projPath,
    [string]$config,
	[string]$projFileName,
	[string]$projName
)
$assemblyPath = $projPath + "Properties\AssemblyInfo.cs"
echo "Post Build Started"
echo "Project Path:" $projPath
echo "Configuration:" $config
echo "Project File Name:" $projFileName
echo "Project Name:" $projName
echo "Assembly Path:" $assemblyPath

#Exits if the build configuration is not release
if ($config -ne "Release")
{
	echo "Not building for release configuration. Exiting."
    exit 0
}

#changes the directory to the project path
cd $projPath
echo "Current Directory:" $pwd

#Gets the assembly version in the assembly file
$pattern = '\[assembly: AssemblyVersion\("(.*)"\)\]'
(Get-Content $assemblyPath) | ForEach-Object{
     if($_ -match $pattern){
         $version = $matches[1]
		 echo "Version:" $version
     }
 }
 
 # Checks if file version is an empty string
 If([string]::IsNullOrEmpty($version)){
	throw [System.Exception] "No version found in AssemblyInfo file."
 }

 #Creates a nupkg file
 nuget pack $projFileName -Properties Configuration=Release;
 #Pushes the nupkg file to the Frazier nuget source
 nuget push $($projName + "." + $version + ".nupkg") -Source "Frazier"
 
 echo "Post Build Finished"