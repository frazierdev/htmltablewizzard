﻿namespace HtmlTableWizzard
{
    /// <summary>
    /// Provides a number of static methods that make it easier to parse variable data into HTML tags for the TableWizz.
    /// </summary>
    public static class HtmlTags
    {
        /// <summary>
        /// Returns an html anchor tag for the given url and text using the a tag.
        /// </summary>
        /// <param name="url">Where the anchor is pointing.</param>
        /// <param name="text">The text displayed.</param>
        /// <returns>An html formatted anchor.</returns>
        public static string Anchor(string url, string text) => $"<a href=\"{url}\">{text}</a>";

        /// <summary>
        /// Bolds the given text using the b tag.
        /// </summary>
        /// <param name="text">The text to add bold tag to.</param>
        /// <returns>HTML formatted text for bolding.</returns>
        public static string Bold(string text) => $"<b>{text}</b>";

        /// <summary>
        /// Italicises the given text using the i tag.
        /// </summary>
        /// <param name="text">The text to italicise.</param>
        /// <returns>HTML formatted italicised text.</returns>
        public static string Italic(string text) => $"<i>{text}</i>";

        /// <summary>
        /// Highlights(marks) the given text using the mark tag. 
        /// </summary>
        /// <param name="text">The text to highlight.</param>
        /// <returns>HTML formatted highlighted text.</returns>
        public static string Mark(string text) => $"<mark>{text}</mark>";

        /// <summary>
        /// Strikes through the given text using the del tag.
        /// </summary>
        /// <param name="text">The text to strike through.</param>
        /// <returns>HTML formatted striked through text.</returns>
        public static string Deleted(string text) => $"<del>{text}</del>";

        /// <summary>
        /// Underscores the given text using the HTML ins tag.
        /// </summary>
        /// <param name="text">The text to underscore.</param>
        /// <returns>HTML formatted underscored text.</returns>
        public static string Inserted(string text) => $"<ins>{text}</ins>";

        /// <summary>
        /// Subscripts the given text using the HTML sub tag.
        /// </summary>
        /// <param name="text">The text to subscript</param>
        /// <returns>HTML formatted subscript text.</returns>
        public static string Subscript(string text) => $"<sub>{text}</sub>";

        /// <summary>
        /// Superscripts the given text using the HTML sup tag.
        /// </summary>
        /// <param name="text">The text to superscript</param>
        /// <returns></returns>
        public static string Superscript(string text) => $"<sup>{text}</sup>";

        /// <summary>
        /// Paragraph elements add some space before and after each p tag.
        /// </summary>
        /// <param name="text">The text inside the paragraph</param>
        /// <returns></returns>
        public static string Paragraph(string text) => $"<p>{text}</p>";

        /// <summary>
        /// The img tag defines a reference to an image in an HTML page.
        /// </summary>
        /// <param name="src">Specifies the URL of an image</param>
        /// <param name="alt">Specifies an alternate text for an image</param>
        /// <returns></returns>
        public static string Image(string src, string alt = "") => $"<img src=\"{src}\" alt=\"{alt}\">";
    }
}
