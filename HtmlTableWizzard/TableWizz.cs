﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace HtmlTableWizzard
{
    public class TableWizz
    {
        public const string table = "<table>";
        public const string tableEnd = "</table>";
        public const string thead = "<thead>";
        public const string theadEnd = "</thead>";
        public const string th = "<th>";
        public const string thEnd = "</th>";
        public const string tbody = "<tbody>";
        public const string tbodyEnd = "</tbody>";
        public const string tfoot = "<tfoot>";
        public const string tfootEnd = "</tfoot>";
        public const string tr = "<tr>";
        public const string trEnd = "</tr>";
        public const string td = "<td>";
        public const string tdEnd = "</td>";
        public const string b = "<b>";
        public const string bEnd = "</b>";

        string Tr(string style) => $"<tr style=\"{style}\">";
        string Td(string style) => $"<td style=\"{style}\">";

        public string Ampersand => "&amp;";
        public string Copyright => "&copy;";
        public string RegisteredTrademark => "&reg;";

        public Queue<string> StyleQueue
        {
            private set;
            get;
        } = new Queue<string>();


        [Obsolete("The Style enum is obsolete and will eventually be removed.  Please start using the CSS style methods.", false)]
        public enum Style
        {
            AlternatingRowColor,
            Basic,
            None
        }
                
        /// <summary>
        /// Generates a standard html table
        /// </summary>
        /// <param name="headers">The table headers</param>
        /// <param name="rows">The rows</param>
        /// <returns>The html formatted table</returns>
        public string GenerateStandardTable(List<string> headers, List<List<string>> rows, List<List<string>> footers = null)
        {
            string html = table + thead + tr;
            headers.ForEach(p => html += $"{th}{p}{thEnd}");
            html += trEnd + theadEnd + tbody;

            rows.ForEach(row =>
            {
                html += StyleQueue.Any() ? Tr(StyleQueue.Dequeue() ?? "") : tr;
                row.ForEach(cellData => html += $"{td}{cellData}{tdEnd}");
                html += trEnd;
            });

            if (footers != null)
            {
                html += tbodyEnd + tfoot;
                footers.ForEach(footer =>
                {
                    html += StyleQueue.Any() ? Tr(StyleQueue.Dequeue() ?? "") : tr;
                    footer.ForEach(cellData => html += $"{td}{cellData}{tdEnd}");
                    html += trEnd;
                });
                html += tfootEnd + tableEnd;
            }
            else
            {
                html += tbodyEnd + tableEnd;
            }

            return html;
        }

        /// <summary>
        /// Generates a pivoted table.  The values in the first columns are bold.
        /// </summary>
        /// <param name="rows">The rows</param>
        /// <param name="headers">The headers(optional)</param>
        /// <returns>The html formated pivoted table</returns>
        public string GeneratePivotedTable(List<List<string>> rows, List<string> headers=null)
        {
            string html = table;
            if(headers != null)
            {
                html += GenerateTheader(headers);
            }
            html += tbody;
            rows.ForEach(p =>
            {
                html += tr;
                p.ForEach(pp =>
                {
                    if (!pp.Equals(p.First()))
                    {
                        html += $"{td}{pp}{tdEnd}";
                    }
                    else
                    {
                        html += $"{Td("text-align: right;")}{b}{pp}{bEnd}{tdEnd}";
                    }
                });
                html += trEnd;
            });
            return html + tableEnd;
        }

        /// <summary>
        /// Generates a header
        /// </summary>
        /// <param name="headers">The headers</param>
        /// <returns>HTML formatted thead with header row.</returns>
        private string GenerateTheader(List<string> headers)
        {
            string html = thead + tr;
            headers.ForEach(p => html += $"{th}{p}{thEnd}");
            html += trEnd + theadEnd;
            return html;
        }


        [Obsolete("The GetStyle method is obsolete and will not be updated.  Please use the newer CSS methods to retrieve CSS styling.", false)]
        /// <summary>
        /// Generates an html style tag that applys to html table elements.  This should be added in a string before the table string is added.
        /// </summary>
        /// <param name="style">HtmlTableWizzard.Style</param>
        /// <returns>HTML formatted style element.</returns>
        public string GetStyle(Style style)
        {
            switch(style)
            {
                case Style.AlternatingRowColor:
                    return
                        "<style type='text/css'>" +
                        "table {border-collapse: collapse;}" +
                        "table, th, td {border: none;}" +
                        "tr>th {border-bottom: 1px solid black;}" +
                        "th,td {text-align: center; padding: 5px;}" +
                        "tr:nth-child(even) {background-color: #E1E1E3}" +
                        "</style>";

                case Style.Basic:
                    return
                        "<style type='text/css'>" +
                        "table {border-collapse: collapse;}" +
                        "table, th, td {border: 1px solid black;}" +
                        "th,td {text-align: center; padding: 5px;}" +
                        "</style>";

                case Style.None:
                    return "";

                default: return "";
            }
        }

        /// <summary>
        /// CSS surrounded in HTML style tag that will allow you to style a table with alternating row colors.  This should be added in a string before the table string is added.
        /// </summary>
        /// <param name="borderCollapse">CSS table border-collapse variable</param>
        /// <param name="border">CSS table, th, td border variable</param>
        /// <param name="borderBottom">CSS tr>th border-bottom variable</param>
        /// <param name="padding">CSS th, td variable</param>
        /// <param name="evenColor">CSS even nth child color variable</param>
        /// <param name="oddColor">CSS odd nth child color variable</param>
        /// <returns>CSS surrounded in HTML style tag that will allow you to style a table with alternating row colors.</returns>
        public string TableCssAlternatingRowColor(string borderCollapse = "collapse", string border = "none", string borderBottom = "1px solid black", string padding = "5px", string evenColor= "#E1E1E3", string oddColor = "#FFFFFF") => "<style type='text/css'>" +
                        $"table {{border-collapse: {borderCollapse};}}" +
                        $"table, th, td {{border: {border};}}" +
                        $"tr>th {{border-bottom: {borderBottom};}}" +
                        $"th,td {{text-align: center; padding: {padding};}}" +
                        $"tr:nth-child(even) {{background-color: {evenColor}}}" +
                        $"tr:nth-child(odd) {{background-color: {oddColor}}}" +
                        "</style>";

        /// <summary>
        /// CSS surrounded in HTML style tag that will give you very basic table styling.  This should be added in a string before the table string is added.
        /// </summary>
        /// <param name="borderCollapse">CSS table border-collapse variable</param>
        /// <param name="border">CSS table, th, td border variable</param>
        /// <param name="padding">CSS th, td variable</param>
        /// <returns>CSS surrounded in HTML style tag that will give you very basic table styling.</returns>
        public string TableCssBasic(string borderCollapse = "collapse", string border = "1px solid black", string padding = "5px") => "<style type='text/css'>" +
                        $"table {{border-collapse: {borderCollapse};}}" +
                        $"table, th, td {{border: {border};}}" +
                        $"th,td {{text-align: center; padding: {padding};}}" +
                        "</style>";
    }
}
